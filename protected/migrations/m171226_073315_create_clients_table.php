<?php

class m171226_073315_create_clients_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('clients', [
            'id' => 'pk',
            'client_snp' => 'string',
            'client_phone' => 'string',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ]);
	}

	public function down()
	{
        $this->dropTable('clients');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}