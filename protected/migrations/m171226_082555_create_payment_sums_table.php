<?php

class m171226_082555_create_payment_sums_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('payment_sums', [
            'id' => 'pk',
            'payment_day_id' => 'integer',
            'client_id' => 'integer',
            'payment_sum' => 'float',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ]);
	}

	public function down()
	{
        $this->dropTable('payment_sums');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}