<?php

class m171226_075000_create_payment_days_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('payment_days', [
            'id' => 'pk',
            'administrator_id' => 'integer',
            'begin_saldo' => 'float',
            'end_saldo' => 'float',
            'turnover' => 'float',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
        ]);
	}

	public function down()
	{

        $this->dropTable('payment_days');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}