<?php

class m171226_071707_create_users_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('users', [
            'id' => 'pk',
            'username' => 'string',
            'password' => 'string',
        ]);
	}

	public function down()
	{
        $this->dropTable('users');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}