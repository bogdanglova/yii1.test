<?php

/**
 * This is the model class for table "payment_days".
 *
 * The followings are the available columns in table 'payment_days':
 * @property integer $id
 * @property integer $administrator_id
 * @property double $begin_saldo
 * @property double $end_saldo
 * @property double $turnover
 * @property string $created_at
 * @property string $updated_at
 */
class PaymentDays extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment_days';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('administrator_id', 'numerical', 'integerOnly'=>true),
			array('begin_saldo, end_saldo, turnover', 'numerical'),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, administrator_id, begin_saldo, end_saldo, turnover, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'administrator'=>array(self::BELONGS_TO,'Users','administrator_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'administrator_id' => 'Administrator',
			'begin_saldo' => 'Begin Saldo',
			'end_saldo' => 'End Saldo',
			'turnover' => 'Turnover',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('administrator_id',$this->administrator_id);
		$criteria->compare('begin_saldo',$this->begin_saldo);
		$criteria->compare('end_saldo',$this->end_saldo);
		$criteria->compare('turnover',$this->turnover);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentDays the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
