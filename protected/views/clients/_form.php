<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'clients-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'client_snp'); ?>
		<?php echo $form->textField($model,'client_snp',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'client_snp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_phone'); ?>
		<?php echo $form->textField($model,'client_phone',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'client_phone'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->