<?php
/* @var $this ClientsController */
/* @var $data Clients */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_snp')); ?>:</b>
	<?php echo CHtml::encode($data->client_snp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_phone')); ?>:</b>
	<?php echo CHtml::encode($data->client_phone); ?>
	<br />




</div>