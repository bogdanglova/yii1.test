<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */

$this->breadcrumbs=array(
    'Payment Days'
);

$this->menu=array(
    array('label'=>'List PaymentDays', 'url'=>array('index')),
    array('label'=>'Create PaymentDays', 'url'=>array('paymentDays/create')),
);

?>

<h1>Manage Payment Days</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'payment-days-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'administrator.username',
        'begin_saldo',
        'turnover',
        'end_saldo',
//        'created_at',
//        'updated_at',
//        array(
//            'class'=>'CButtonColumn',
//        ),
    ),
)); ?>
