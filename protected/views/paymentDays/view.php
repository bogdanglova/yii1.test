<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */

$this->breadcrumbs=array(
	'Payment Days'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PaymentDays', 'url'=>array('index')),
	array('label'=>'Create PaymentDays', 'url'=>array('create')),
	array('label'=>'Update PaymentDays', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PaymentDays', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaymentDays', 'url'=>array('admin')),
);
?>

<h1>View PaymentDays #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'administrator_id',
		'begin_saldo',
		'end_saldo',
		'turnover',
		'created_at',
		'updated_at',
	),
)); ?>
