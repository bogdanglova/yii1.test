<?php
/* @var $this PaymentDaysController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payment Days',
);

$this->menu=array(
	array('label'=>'Create PaymentDays', 'url'=>array('create')),
	array('label'=>'Manage PaymentDays', 'url'=>array('admin')),
);
?>

<h1>Payment Days</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
