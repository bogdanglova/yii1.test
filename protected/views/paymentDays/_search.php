<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'administrator_id'); ?>
		<?php echo $form->textField($model,'administrator_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'begin_saldo'); ?>
		<?php echo $form->textField($model,'begin_saldo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_saldo'); ?>
		<?php echo $form->textField($model,'end_saldo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'turnover'); ?>
		<?php echo $form->textField($model,'turnover'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->