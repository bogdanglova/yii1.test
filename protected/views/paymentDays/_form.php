<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */
/* @var $form CActiveForm */
$csrfToken = Yii::app()->request->csrfToken;
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'payment-days-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    ));
    $model=new Users();
    $authors=Users::model()->findAll();
    $items=CHtml::listData(Users::model()->findAll(),'id','username');
    echo $form->dropDownList($model,'username',$items,array('empty'=>'Select Option'));
    $model=new PaymentDays();
    ?>


    <div class="row">
        <?php echo $form->labelEx($model,'begin_saldo'); ?>
        <?php echo $form->textField($model,'begin_saldo',array('size'=>60,'maxlength'=>255,'value'=>0)); ?>
        <?php echo $form->error($model,'begin_saldo'); ?>
    </div>
    <?php $this->endWidget(); ?>
    <?=CHtml::button('Добавить',['class' => 'btn btn-primary','id'=>'add', 'name' => 'save-button']) ?>
    <p></p>
    <div class="payment-day-index">
        <table id="myTable" class="table table-striped table-bordered">
            <thead>
            <th>Клиент</th>
            <th>Сумма оплаты</th>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id'=>'payment-days-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    ));
    ?>
    <div class="row">
        <?php echo $form->textField($model,'turnover',array('size'=>60,'maxlength'=>255,'readonly'=>true)); ?>
    </div>
    <div class="row">
        <?php echo $form->textField($model,'end_saldo',array('size'=>60,'maxlength'=>255,'readonly'=>true)); ?>
    </div>
    <?php $this->endWidget(); ?>

    <?=CHtml::button('Сохранить',['class' => 'btn btn-primary','id'=>'save', 'name' => 'save-button']) ?>

</div><!-- form -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $('#payment-days-form').submit(function () {
        return false;
    });
    var i=0;
    $('#add').on('click', function () {
        i++;
        $('#myTable > tbody:last').append('<tr><td class="edit"><input id="edit'+i+'"/></td><td class="edit"><input class="sum" id="edit1'+i+'"/></td></tr>');
    });
    $('input').on('focus', function () {
        var k = 0;
        for (var j = 0; j < i;) {
            j++;
            k += parseFloat($('#edit1' + j).val())
        }
        $('#PaymentDays_turnover').val(k);
        $('#PaymentDays_end_saldo').val(k+parseFloat($('#PaymentDays_begin_saldo').val()));

    });

    $('#save').on('click',function () {
        var mass1=[];
        var k=0;
        for (var j = 0; j < i;) {
            j++;
            k += parseFloat($('#edit1' + j).val())
        }
        $('#PaymentDays_turnover').val(k);
        $('#PaymentDays_end_saldo').val(k+parseFloat($('#PaymentDays_begin_saldo').val()));
        k=0;
        for(j=1;j<=i;j++){
            mass1[k]=$('#edit'+j).val();
            k++;
        }
        var mass2=[];
        k=0;
        for(j=1;j<=i;j++){
            mass2[k]=parseFloat($('#edit1'+j).val());
            k++;
        }
        var data=[mass1,mass2];
        var data2=[$('#Users_username option:selected').val(),parseFloat($('#PaymentDays_begin_saldo').val()),parseFloat( $('#PaymentDays_turnover').val()),parseFloat( $('#PaymentDays_end_saldo').val())]
        $.ajax({
            url:'/paymentDays/create',
            data:{data1:data,data2:data2,size:i},
            dataType: 'json',
            type:'POST',
            success:function (data) {
                console.log(data);
                $(location).attr("href", '/');
            }
        });

    });
</script>