<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */

$this->breadcrumbs=array(
	'Payment Days'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PaymentDays', 'url'=>array('index')),
	array('label'=>'Manage PaymentDays', 'url'=>array('admin')),
);
?>

<h1>Create PaymentDays</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>