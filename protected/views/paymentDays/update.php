<?php
/* @var $this PaymentDaysController */
/* @var $model PaymentDays */

$this->breadcrumbs=array(
	'Payment Days'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PaymentDays', 'url'=>array('index')),
	array('label'=>'Create PaymentDays', 'url'=>array('create')),
	array('label'=>'View PaymentDays', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PaymentDays', 'url'=>array('admin')),
);
?>

<h1>Update PaymentDays <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>