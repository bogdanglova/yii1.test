<?php
/* @var $this PaymentDaysController */
/* @var $data PaymentDays */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('administrator_id')); ?>:</b>
	<?php echo CHtml::encode($data->administrator_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('begin_saldo')); ?>:</b>
	<?php echo CHtml::encode($data->begin_saldo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_saldo')); ?>:</b>
	<?php echo CHtml::encode($data->end_saldo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('turnover')); ?>:</b>
	<?php echo CHtml::encode($data->turnover); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />


</div>